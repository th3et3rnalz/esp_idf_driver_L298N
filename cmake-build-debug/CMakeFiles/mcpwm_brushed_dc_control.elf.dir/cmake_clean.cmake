file(REMOVE_RECURSE
  "bootloader/bootloader.bin"
  "bootloader/bootloader.elf"
  "bootloader/bootloader.map"
  "config/sdkconfig.cmake"
  "config/sdkconfig.h"
  "flash_project_args"
  "mcpwm_brushed_dc_control.bin"
  "mcpwm_brushed_dc_control.map"
  "project_elf_src_esp32.c"
  "CMakeFiles/mcpwm_brushed_dc_control.elf.dir/project_elf_src_esp32.c.obj"
  "mcpwm_brushed_dc_control.elf"
  "mcpwm_brushed_dc_control.elf.pdb"
  "project_elf_src_esp32.c"
)

# Per-language clean rules from dependency scanning.
foreach(lang C)
  include(CMakeFiles/mcpwm_brushed_dc_control.elf.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
